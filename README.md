# Library - Context Loader (cllib)
Dependency injection library. It was created to avoid having to use a spring-context that is unnecessarily complex and large for my needs.\
This library can do basic things and is a very small jar file.

## Requirements
Java 11+

## Download
1) Go to the **Tags** section.
2) Find the latest version of the app.
3) Click the Download button -> Download artifacts -> package-release (it is a zip file, then unzip it).

## Use
There are **3 annotations**:
1) **ContextClass**
    - It is used to indicate the class to be included in dependency injection. This class can then be called (eg
     using the Inject annotation).
    - This annotation only applies to classes (implementations).
2) **PostCOnstruct**
    - Annotation for a method in a class that is annotated as ContextClass.
    - This method is called automatically after the class instance has been created.
    - A class can have multiple of these methods.
    - This method can have parameters containing context classes (similar to Inject).
3) **Inject**
    - Annotation for class instance variables. Thus, the variable that will be marked with this annotation will get the instance from the context.

## Events
There are **2 basic types of events**:
1) **ContextLoaderEvent**
    - You can capture various events such as scanning packages, finding a class, creating an instance, etc.
2) **ReadyEvent**
    - This event is called immediately after the context is ready for use.
    - In this event, the logic of the application (application launch) should be.

## Configuration
Configuration is done using context loader definition. **This library has no configuration file.**

## Dependencies
There are no. It is pure JDK.

## Example
Can be found here: [LINK](https://gitlab.com/OphiteCorp/library---context-loader/blob/master/src/test/java/oc/mimic/lib/contextloader/test/Application.java)
