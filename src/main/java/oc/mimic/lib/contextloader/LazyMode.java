package oc.mimic.lib.contextloader;

/**
 * Typ lazy načtení context tříd.
 *
 * @author mimic
 */
public enum LazyMode {

  /**
   * Normální mód, kde lazy=False znamená, že třída bude automaticky načtena po dokončení skenu všech package.
   */
  NORMAL,

  /**
   * Neguje nastavení. Třídy, které mají lazy=False, tak nebudou načteny. Třídy které nemají lazy vůbec nebo lazy=True,
   * tak budou načteny automaticky po dokončení skenu všech package.
   */
  NEGATE,

  /**
   * Všechny context třídy nastaví jako lazy=False. Ignoruje nastavení u jednotlivých tříd a všechny třídy budou načteny
   * po dokončení skenu všech package.
   */
  ALL
}
