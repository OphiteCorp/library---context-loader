package oc.mimic.lib.contextloader;

import oc.mimic.lib.contextloader.annotation.ContextClass;
import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;
import oc.mimic.lib.contextloader.exception.ContextLoaderException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Executable;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Modifier;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Implementace context loaderu, která představuje řešení pro vlastní dependency injection.
 *
 * @author mimic
 */
public final class ContextLoader implements IContextLoader {

  private static ContextLoader instance;

  // control pro správu událostí
  private final ContextLoaderEventControl eventControl;
  // drží všechny context třídy - plní se v průběhu skenování a drží instance tříd
  private final Map<String, ContextClassData> contextClasses;
  // nastavení pro context loader
  private ContextLoaderSettings settings;

  // zámek a stav skenování
  private final Object stateLock = new Object();
  private ContextScanState state = ContextScanState.READY;

  // callback, který se zavolá po kompletním načtení všech package
  private IContextLoaderReadyCallback readyCallback;

  private ContextLoader() {
    eventControl = new ContextLoaderEventControl(this);
    contextClasses = new LinkedHashMap<>();
    settings = new ContextLoaderSettings(); // instance s výchozím nastavením
  }

  /**
   * Získá instanci context loaderu.
   *
   * @return Instance context loaderu.
   */
  public static synchronized ContextLoader getInstance() {
    if (instance == null) {
      instance = new ContextLoader();
    }
    return instance;
  }

  /**
   * Zahájí skenování package. Vyhledá všechny třídy, které mají anotaci {@link ContextClass}.<br>
   * Tato metoda se musí volat jako poslední, protože používá nastavení jako jsou listenery, callbacky apod.,
   * který by měli být dostupný před spuštěním skenování.<br>
   * Tato metoda by se měla zavolat pouze jednou.
   *
   * @param packageNames Seznam package.
   */
  public void scan(String... packageNames) {
    synchronized (stateLock) {
      // sken lze spustit jen jednou
      if (getContextState() != ContextScanState.READY) {
        throw new ContextLoaderException("The package scan can only be run once.");
      }
      state = ContextScanState.SCANNING;

      if (packageNames != null && packageNames.length > 0) {
        for (var packageName : packageNames) {
          if (packageName == null) {
            throw new ContextLoaderException("Package name cannot be null.");
          }
          eventControl.firePackageScanStarted(packageName);
          try {
            // získá úplně všechny třídy z package
            var classes = ContextLoaderHelper.getClassesFromPackage(packageName);
            eventControl.firePackageScanComplete(packageName, classes);

            for (var clazz : classes) {
              // pokud je třída platná pro context loader, tak ji přidá do contextu
              if (isValidContextClass(clazz)) {
                addToContextMap(clazz);
              }
            }
          } catch (ContextLoaderException e) {
            throw e;
          } catch (Exception e) {
            throw new ContextLoaderException("There was an error scanning the package: " + packageName, e);
          }
        }
        state = ContextScanState.DONE;

        // sken všech package byl dokončen, nyní můžeme automaticky vytvořit instanci non-lazy tříd
        var lazyClasses = contextClasses.values().stream().filter(p -> {
          var apply = false;
          switch (getSettings().getLazyMode()) {
            case NORMAL:
              apply = !p.isLazy();
              break;
            case NEGATE:
              apply = p.isLazy();
              break;
            case ALL:
              apply = true;
              break;
          }
          return apply;
        }).sorted(Comparator.comparingInt(ContextClassData::getLazyOrder)).collect(Collectors.toList());

        for (var contextClass : lazyClasses) {
          internalGet(contextClass.getContextClass());
        }
        fireContextReady();
      } else {
        throw new ContextLoaderException("The input parameter 'packageNames' is null or empty.");
      }
    }
  }

  @Override
  public boolean isValidContextClass(Class<?> clazz) {
    if (clazz == null) {
      return false;
    }
    var has = true;
    has &= clazz.isAnnotationPresent(ContextClass.class);
    has &= !clazz.isInterface();
    has &= !Modifier.isAbstract(clazz.getModifiers());

    if (has) {
      // pokud je třída pro context platná a má nějakou dědičnost, tak projdeme všechny rodiče, jestli neobsahují
      // context anotaci - pokud ano, tak výjímka
      if (ContextLoaderHelper.hasSuperClass(clazz)) {
        if (isAnyParentClassWithContext(clazz.getSuperclass())) {
          throw new ContextLoaderException(
                  "Class '" + clazz.getName() + "' inherits from a class that already contains a '" +
                  ContextClass.class.getSimpleName() + "' annotation.");
        }
      }
    }
    return has;
  }

  @Override
  public ContextScanState getContextState() {
    return state;
  }

  @Override
  public synchronized Object get(Class<?> contextClass) {
    if (getContextState() != ContextScanState.DONE) {
      throw new ContextLoaderException("The get(Class<?>) method can be called after the scan is complete.");
    }
    return internalGet(contextClass);
  }

  @Override
  public synchronized Object get(String contextClassName) {
    if (getContextState() != ContextScanState.DONE) {
      throw new ContextLoaderException("The get(String) method can be called after the scan is complete.");
    }
    return internalGet(contextClassName);
  }

  /**
   * Vnitří "get" pro získání instance třídy.
   *
   * @param contextClassName Název třídy v contextu.
   *
   * @return Instance třídy nebo null.
   */
  private synchronized Object internalGet(String contextClassName) {
    var contextClassData = getContextClassData(contextClassName);
    if (contextClassData != null) {
      // pokud třída v contextu bude existovat, ale nebude existovat její instanci, tak jí prvně vytvoří včetně všech
      // závislostí, které na sobě má
      if (contextClassData.getInstance() == null) {
        initContextClass(contextClassData.getContextClass());
        callContextClassObjects(contextClassData);
        return internalGet(contextClassName);
      }
      return contextClassData.getInstance();
    }
    return null;
  }

  /**
   * Vnitří "get" pro získání instance třídy.
   *
   * @param contextClass Typ třídy v contextu.
   *
   * @return Instance třídy nebo null.
   */
  private synchronized <T> Object internalGet(Class<T> contextClass) {
    if (contextClass != null) {
      if (contextClass.isInterface()) {
        // pokud je vstupní třída rozhraní, tak najde odpovídající implementaci - pokud je implementací více,
        // tak vyhodí výjimku, jinak vrátí název třídy v contextu
        var contextClassName = findContextClassNameByInterface(contextClass);
        if (contextClassName != null) {
          return internalGet(contextClassName);
        }
      } else {
        // vstupní třída není rozhraní, ale třída nebo jiný objekt
        for (var contextClassData : contextClasses.values()) {
          if (contextClassData.getContextClass().equals(contextClass)) {
            return internalGet(contextClassData.getName());
          }
        }
      }
    }
    return null;
  }

  @Override
  public List<IContextClassData> getAllContexts() {
    if (getContextState() != ContextScanState.DONE) {
      throw new ContextLoaderException("The getAllContexts() method can be called after the scan is complete.");
    }
    return new ArrayList<>(contextClasses.values());
  }

  /**
   * Získá nastavení context loaderu.
   *
   * @return Nastavení.
   */
  public ContextLoaderSettings getSettings() {
    return settings;
  }

  /**
   * Nastaví context loader.
   *
   * @param settings Nastavení.
   */
  public void setSettings(ContextLoaderSettings settings) {
    if (settings == null) {
      throw new ContextLoaderException("The input setting cannot be null.");
    }
    this.settings = settings;
  }

  /**
   * Nastaví ready callback, který se zavolá ve chvíli, kdy je sken dokončen.
   *
   * @param readyCallback Instance callbacku.
   */
  public void setReadyCallback(IContextLoaderReadyCallback readyCallback) {
    if (readyCallback == null) {
      throw new ContextLoaderException("Input parameter 'readyCallback' must not be null.");
    }
    this.readyCallback = readyCallback;
  }

  /**
   * Přidá listener pro zachycení událostí skenu. Nepodporuje přidání více stejných instancí.
   *
   * @param listener Instance listeneru.
   */
  public synchronized void addEventListener(IContextLoaderEventListener listener) {
    eventControl.addEventListener(listener);
  }

  /**
   * Odebere listener pro zachycení událostí skenu.
   *
   * @param listener Instance listeneru.
   *
   * @return True, pokud byl listener odebrán.
   */
  public synchronized boolean removeEventListener(IContextLoaderEventListener listener) {
    return eventControl.removeEventListener(listener);
  }

  /**
   * Událost - context byl načten a je připraven k použití.
   */
  private synchronized void fireContextReady() {
    if (readyCallback != null) {
      readyCallback.contextReady(this);
    }
  }

  /**
   * Vyhledá context třídu podle rozhraní. Pouze jedna třída musí mít toto rozhraní, pokud ne, tak výjimka.
   *
   * @param interfaceClass Rozhraní.
   *
   * @return Název třídy v contextu.
   */
  private String findContextClassNameByInterface(Class<?> interfaceClass) {
    if (interfaceClass == null || !interfaceClass.isInterface()) {
      return null;
    }
    // jako <String> drží název root třídy v contextu, ve které bylo nalezeno rozhraní - to znamená, že root bude vždy
    // třída v contextu od které probíhá vyhledávání (vždy contextClassData)
    // je to z důvodu, že třída může dědit jinou, která bude mít toto rozhraní
    var counter = new HashSet<String>(1);

    for (var contextClassData : contextClasses.values()) {
      // tato metoda se volá rekurzivně - kvůli dědičnosti
      findContextClassNameByInterfaceRec(contextClassData.getName(), contextClassData.getContextClass(), interfaceClass,
              counter);
    }
    if (counter.size() == 1) {
      // pokud je counter 1, tak v celém contextu existuje pouze 1 implementace rozhraní - je OK
      return counter.stream().findFirst().get();

    } else if (counter.size() > 1) {
      // existuje více implementací pro jedno rozhraní - to je sice běžný stav, ale context loader není schopný zjistit,
      // která implementace je ta správná, kterou chceme
      throw new ContextLoaderException("The interface '" + interfaceClass.getName() +
                                       "' contains multiple classes in the context. Classes are: " +
                                       Arrays.toString(counter.toArray()));
    }
    return null;
  }

  /**
   * Vyhledá počet tříd v contextu, které mají vstupní rozhraní.
   *
   * @param rootContextClassName Název root třídy v contextu. Od této třídy probíhá vyhledávání.
   * @param clazz                Aktuální třída v contextu. První průchod bude stejný jako root.
   * @param interfaceClass       Třída s rozhraním, pro kterou hledáme implementaci.
   * @param counter              Počítadlo (rozhraní může mít více tříd).
   */
  private void findContextClassNameByInterfaceRec(String rootContextClassName, Class<?> clazz, Class<?> interfaceClass,
          Set<String> counter) {

    if (ContextLoaderHelper.hasSuperClass(clazz)) {
      // pokud má třída rodiče (dědi z jiné třídy)
      findContextClassNameByInterfaceRec(rootContextClassName, clazz.getSuperclass(), interfaceClass, counter);
    }
    for (var iFace : clazz.getInterfaces()) {
      if (interfaceClass.equals(iFace)) {
        // bylo nalezeno rozhraní
        counter.add(rootContextClassName);
        break;
      }
    }
  }

  /**
   * Přidá třídu do context mapy. Pokud v context mapě taková třída již existuje, tak vyhazuje výjímku.
   *
   * @param contextClass Třída, která půjde do contextu.
   */
  private void addToContextMap(Class<?> contextClass) {
    var contextClassName = getContextClassName(contextClass);
    if (!contextClasses.containsKey(contextClassName)) {
      var contextClassData = new ContextClassData(contextClassName, contextClass);
      eventControl.fireContextClassFound(contextClassData);
      contextClasses.put(contextClassName, contextClassData);
    } else {
      var clazz = contextClasses.get(contextClassName).getContextClass();
      throw new ContextLoaderException(
              "The '" + contextClassName + "' context class name already exists in the context under: " +
              clazz.getName());
    }
  }

  /**
   * Zjistí, zda vstupní třída dědí z nějaké třídy, která již obsahuje anotaci {@link ContextClass}.
   *
   * @param parentClass Rodičovská třída třídy, pro kterou chceme vyhledat anotaci.
   *
   * @return True, pokud nějaká rodičovská třída má anotaci.
   */
  private boolean isAnyParentClassWithContext(Class<?> parentClass) {
    var stack = new Stack<Class<?>>();
    stack.push(parentClass);

    while (!stack.isEmpty()) {
      var popClass = stack.pop();

      if (ContextLoaderHelper.hasSuperClass(popClass)) {
        stack.push(popClass.getSuperclass());
      }
      if (popClass.isAnnotationPresent(ContextClass.class)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Získá název třídy v contextu tříd.
   *
   * @param clazz Třída, která má anotaci {@link ContextClass}.
   *
   * @return Název třídy v contextu. Pokud se nejedná o třídu v contextu, tak vrací null.
   */
  private String getContextClassName(Class<?> clazz) {
    if (clazz != null && clazz.isAnnotationPresent(ContextClass.class)) {
      var cc = clazz.getAnnotation(ContextClass.class);

      if (cc.value().strip().length() > 0) {
        return cc.value();
      } else {
        if (getSettings().isGenerateUniqueNames()) {
          return ContextLoaderHelper.createClassHash(clazz);
        } else {
          return clazz.getName();
        }
      }
    }
    return null;
  }

  /**
   * Získá informace o context třídě v contextu.
   *
   * @param clazz Třída.
   *
   * @return Informace o třídě v contextu. Pokud třída nebude existovat, tak vrací null.
   */
  private ContextClassData getContextClassData(Class<?> clazz) {
    var contextClassName = getContextClassName(clazz);
    return getContextClassData(contextClassName);
  }

  /**
   * Získá informace o context třídě v contextu.
   *
   * @param contextClassName Název třídy v contextu.
   *
   * @return Informace o třídě v contextu. Pokud třída nebude existovat, tak vrací null.
   */
  private ContextClassData getContextClassData(String contextClassName) {
    if (contextClassName != null && contextClasses.containsKey(contextClassName)) {
      return contextClasses.get(contextClassName);
    }
    return null;
  }

  /**
   * Zpracuje context třídu. V tomto okamžiku jsou všechny package o skenovaný a vstupní třída bude mít vždy anotaci
   * pro context.
   *
   * @param contextClass Třída, která má anotaci pro context loader.
   */
  private void initContextClass(Class<?> contextClass) {
    var contextClassData = getContextClassData(contextClass);

    // pro vstupní context třídu neexistuje instance - bude vytvořena
    if (contextClassData.getInstance() == null) {
      var parametersInstances = new ArrayList<>(0);
      var constructor = findRightConstructor(contextClassData.getContextClass(), parametersInstances);
      eventControl.fireCreatingInstance(contextClassData, constructor);
      constructor.setAccessible(true); // konstruktor nemusí být pouze public

      try {
        // logika pro vytvoření instance třídy
        if (constructor.getParameterCount() == 0) {
          contextClassData.setInstance(constructor.newInstance());
        } else {
          contextClassData.setInstance(constructor.newInstance(parametersInstances.toArray()));
        }
        eventControl.fireInstanceCreated(contextClassData, constructor);

      } catch (Exception e) {
        throw new ContextLoaderException(
                "It was not possible to create an instance of class: " + contextClassData.getContextClass().getName(),
                e);
      }
    }
    if (contextClassData.getInstance() == null) {
      throw new ContextLoaderException("No suitable constructor was found to create an instance of class: " +
                                       contextClassData.getContextClass().getName());
    }
  }

  /**
   * Zavolá všechny objekty v context třídě. Mezi ně patří třeba {@link PostConstruct} nebo {@link Inject}.
   *
   * @param contextClassData Informace o context třídě.
   */
  private void callContextClassObjects(ContextClassData contextClassData) {
    try {
      callInjectFields(contextClassData);
    } catch (Exception e) {
      throw new ContextLoaderException(
              "Could not call fields marked as Inject for: " + contextClassData.getContextClass().getName(), e);
    }
    try {
      callPostConstructMethods(contextClassData);
    } catch (Exception e) {
      throw new ContextLoaderException(
              "Could not call method marked as PostConstruct for: " + contextClassData.getContextClass().getName(), e);
    }
  }

  /**
   * Vyhledá konstruktor v context třídě, přes který je možné vytvořit instanci.
   *
   * @param contextClass   Context třída.
   * @param paramInstances Získá instance parametrů konstruktoru (pokud nějaký jsou).
   *
   * @return Vhodný konstruktor k vytvoření třídy.
   */
  private Constructor<?> findRightConstructor(Class<?> contextClass, List<Object> paramInstances) {
    var constructors = contextClass.getDeclaredConstructors();
    // pokud bude pouze 1 konstruktor a to bez parametru
    if (constructors.length == 1 && constructors[0].getParameterCount() == 0) {
      return constructors[0];
    }
    var customParamMapping = getSettings().getCustomParamMapping();
    Constructor<?> constructor = null;
    // pokud bude vice konstruktorů
    for (var c : constructors) {
      if (c.getParameterCount() == 0) {
        // pokud bude konstruktor bez parametru, tak má přednost
        constructor = c;
        break;
      } else {
        var containsAll = true;
        for (var param : c.getParameters()) {
          if (!customParamMapping.containsKey(param.getType())) {
            containsAll = false;
            break;
          } else {
            var instance = customParamMapping.get(param.getType());
            paramInstances.add(instance);
          }
        }
        // pokud bude konstruktor obsahovat parametry, které je možné naplnit
        if (containsAll) {
          constructor = c;
          break;
        }
      }
    }
    if (constructor == null) {
      if (getSettings().isIgnoreNullReferences()) {
        // pokud se bude ignorovat null parametry v konstruktoru, tak použije první konstruktor a parametry budou null
        constructor = constructors[0];
        for (var i = 0; i < constructor.getParameterCount(); i++) {
          paramInstances.add(null);
        }
      } else {
        throw new ContextLoaderException(
                "A suitable constructor was not found in '" + contextClass.getName() + "' class.");
      }
    }
    return constructor;
  }

  /**
   * Zavolá všechny metody, které jsou označené jako {@link PostConstruct}.
   *
   * @param contextClassData Informace o context třídě. Tato třída již má vytvořenou instanci (musí mít).
   *
   * @throws InvocationTargetException
   * @throws IllegalAccessException
   */
  private void callPostConstructMethods(ContextClassData contextClassData)
          throws InvocationTargetException, IllegalAccessException {

    var queue = new ArrayDeque<Class<?>>(1);
    queue.push(contextClassData.getContextClass());

    // prvně projde všechny rodiče, protože metody se budou volat od nejnižší úrovně
    while (ContextLoaderHelper.hasSuperClass(queue.peek())) {
      queue.push(queue.peek().getSuperclass());
    }
    while (!queue.isEmpty()) {
      var clazz = queue.pop();
      var methods = clazz.getDeclaredMethods();

      for (var method : methods) {
        if (method.isAnnotationPresent(PostConstruct.class)) {
          eventControl.fireCallPostConstruct(contextClassData, clazz, method);
          method.setAccessible(true); // metoda nemusí být pouze public

          if (method.getParameterCount() == 0) {
            // postConstruct metody bez parametru
            method.invoke(contextClassData.getInstance());
          } else {
            var contextParams = getContextParameters(method);
            if (contextParams.size() == method.getParameterCount()) {
              // postConstruct metoda musí mít pouze takové parametry, které je možé inicializovat
              var args = new ArrayList<>(method.getParameterCount());

              for (var param : method.getParameters()) {
                var instance = internalGet(param.getType());
                if (instance == null) {
                  instance = getSettings().getCustomParamMapping().get(param.getType());
                }
                args.add(instance);
              }
              method.invoke(contextClassData.getInstance(), args.toArray());
            } else {
              throw new ContextLoaderException("The '" + clazz.getName() + "#" + method.getName() +
                                               "' method has a parameter that is not placed in context.");
            }
          }
        }
      }
    }
  }

  /**
   * Naplní všechny fieldy, které jsou označené jako {@link Inject}.
   *
   * @param contextClassData Informace o context třídě. Tato třída již má vytvořenou instanci (musí mít).
   *
   * @throws IllegalAccessException
   */
  private void callInjectFields(ContextClassData contextClassData) throws IllegalAccessException {
    var queue = new ArrayDeque<Class<?>>(1);
    queue.push(contextClassData.getContextClass());

    // prvně projde všechny rodiče, protože fieldy se budou volat od nejnižší úrovně
    while (ContextLoaderHelper.hasSuperClass(queue.peek())) {
      queue.push(queue.peek().getSuperclass());
    }
    while (!queue.isEmpty()) {
      var clazz = queue.pop();
      var fields = clazz.getDeclaredFields();

      for (var field : fields) {
        if (field.isAnnotationPresent(Inject.class)) {
          var inject = field.getAnnotation(Inject.class);
          var fieldName = inject.value();
          Object fieldInstance;

          if (fieldName.strip().isBlank()) {
            fieldInstance = internalGet(field.getType());
          } else {
            fieldInstance = internalGet(fieldName);
          }
          if (fieldInstance == null) {
            fieldInstance = getSettings().getCustomParamMapping().get(field.getType());
          }
          if (fieldInstance != null) {
            eventControl.fireCallInject(contextClassData, clazz, field);
            field.setAccessible(true); // field nemusí být pouze public
            field.set(contextClassData.getInstance(), fieldInstance);
          } else {
            if (!getSettings().isIgnoreNullReferences()) {
              throw new ContextLoaderException("It was not possible to obtain '" + field.getType().getName() +
                                               "' class instances to init the Inject field: " + clazz.getName() + "#" +
                                               field.getName());
            }
          }
        }
      }
    }
  }

  /**
   * Získá parametry pro executable (konstruktor, metoda) uložené v contextu. Správně by výstupní počet parametrů měl
   * být shodný s počtem vstupních parametrů v executable.
   *
   * @param executable Konstruktor nebo metoda.
   *
   * @return Seznam tříd pro parametry.
   */
  private List<Class<?>> getContextParameters(Executable executable) {
    var list = new ArrayList<Class<?>>();

    for (var param : executable.getParameters()) {
      var paramClass = param.getType();
      ContextClassData contextClassData;

      // jako parametry v konstruktoru podporujeme i rozhraní
      if (paramClass.isInterface()) {
        var contextClassName = findContextClassNameByInterface(paramClass);
        contextClassData = getContextClassData(contextClassName);
      } else {
        contextClassData = getContextClassData(paramClass);
      }
      if (contextClassData == null) {
        if (getSettings().isIgnoreNullReferences()) {
          list.add(null);
        } else {
          break;
        }
      } else {
        list.add(contextClassData.getContextClass());
      }
    }
    // nějaký parametry nebyly nalezeny v contextu
    if (list.size() != executable.getParameterCount()) {
      for (var param : executable.getParameters()) {
        if (getSettings().getCustomParamMapping().containsKey(param.getType())) {
          list.add(param.getType());
        }
      }
    }
    return list;
  }

  /**
   * Rozšířené informace o třídě v contextu.
   *
   * @author mimic
   */
  private final class ContextClassData implements IContextClassData {

    private final String name;
    private final Class<?> contextClass;
    private final Set<Class<?>> parents;
    private final Set<Class<?>> interfaces;
    private final boolean lazy;
    private final int lazyOrder;
    private Object instance;

    /**
     * Vytvoří nový objekt s informacemi o context třídě.
     *
     * @param name         Název třídy v contextu.
     * @param contextClass Samotná context třída.
     */
    private ContextClassData(String name, Class<?> contextClass) {
      this.name = name;
      this.contextClass = contextClass;

      var a = contextClass.getAnnotation(ContextClass.class);
      lazy = a.lazy();
      lazyOrder = a.lazyOrder();
      parents = getParents(contextClass);
      interfaces = getInterfaces(contextClass);
    }

    @Override
    public String getName() {
      return name;
    }

    @Override
    public Class<?> getContextClass() {
      return contextClass;
    }

    @Override
    public Object getInstance() {
      return instance;
    }

    private void setInstance(Object instance) {
      this.instance = instance;
    }

    public Set<Class<?>> getParents() {
      return parents;
    }

    public Set<Class<?>> getInterfaces() {
      return interfaces;
    }

    @Override
    public boolean isLazy() {
      return lazy;
    }

    @Override
    public int getLazyOrder() {
      return lazyOrder;
    }

    @Override
    public String toString() {
      return name;
    }

    /**
     * Získá všechny rodiče třídy. Prochází i všechny jeho rodiče.
     *
     * @param clazz Vstupní třída.
     *
     * @return Rodiče třídy.
     */
    private Set<Class<?>> getParents(Class<?> clazz) {
      var parents = new HashSet<Class<?>>();
      var parent = clazz;

      while (ContextLoaderHelper.hasSuperClass(parent)) {
        parent = parent.getSuperclass();
        parents.add(parent);
      }
      return parents;
    }

    /**
     * Získá všechny rozhraní třídy. Prochází i všechny rodiče.
     *
     * @param clazz Vstupní třída.
     *
     * @return Rodiče třídy.
     */
    private Set<Class<?>> getInterfaces(Class<?> clazz) {
      var interfaces = new HashSet<Class<?>>();
      var parent = clazz;

      while (ContextLoaderHelper.hasSuperClass(parent)) {
        interfaces.addAll(Arrays.asList(parent.getInterfaces()));
        parent = parent.getSuperclass();
      }
      return interfaces;
    }
  }
}
