package oc.mimic.lib.contextloader;

import java.util.List;

/**
 * Veřejné rozhraní pro context loader.
 *
 * @author mimic
 */
public interface IContextLoader {

  /**
   * Vyhodnotí, zda je třída platná pro context loader a může jí přidat do contextu.
   *
   * @param clazz Vstupní třída.
   *
   * @return True, pokud je možné přidat třídu do contextu.
   */
  boolean isValidContextClass(Class<?> clazz);

  /**
   * Získá stav context loaderu.
   *
   * @return Stav.
   */
  ContextScanState getContextState();

  /**
   * Získá instanci context třídy.
   *
   * @param contextClass Třída v contextu.
   *
   * @return Instance třídy.
   */
  Object get(Class<?> contextClass);

  /**
   * Získá instanci context třídy.
   *
   * @param contextClassName Název třídy v contextu.
   *
   * @return Instance třídy.
   */
  Object get(String contextClassName);

  /**
   * Získá všechny contexty v context loaderu.
   *
   * @return Seznam všech contextů.
   */
  List<IContextClassData> getAllContexts();
}
