package oc.mimic.lib.contextloader.exception;

/**
 * Výjimka pro context loader.
 *
 * @author mimic
 */
public class ContextLoaderException extends RuntimeException {

  /**
   * Vytvoří novou instanci výjimky.
   *
   * @param message Vlastní zpráva.
   */
  public ContextLoaderException(String message) {
    super(message);
  }

  /**
   * Vytvoří novou instanci výjimky.
   *
   * @param message Vlastní zpráva.
   * @param cause   Původní výjimka.
   */
  public ContextLoaderException(String message, Throwable cause) {
    super(message, cause);
  }
}
