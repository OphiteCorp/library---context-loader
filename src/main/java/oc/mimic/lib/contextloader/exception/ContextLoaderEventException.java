package oc.mimic.lib.contextloader.exception;

/**
 * Výjimka pri zpracování události.
 *
 * @author mimic
 */
public final class ContextLoaderEventException extends RuntimeException {

  private static final long serialVersionUID = 6677546911920281661L;

  /**
   * Vytvoří novou instanci výjimky.
   *
   * @param message Vlastní zpráva.
   */
  public ContextLoaderEventException(String message) {
    super(message);
  }

  /**
   * Vytvoří novou instanci výjimky.
   *
   * @param message Vlastní zpráva.
   * @param cause   Původní výjimka.
   */
  public ContextLoaderEventException(String message, Throwable cause) {
    super(message, cause);
  }
}
