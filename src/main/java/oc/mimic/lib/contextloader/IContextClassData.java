package oc.mimic.lib.contextloader;

/**
 * Základní instance o třídě v contextu.
 *
 * @author mimic
 */
public interface IContextClassData {

  /**
   * Třída, která je umístěna v contextu.
   *
   * @return Context třída.
   */
  Class<?> getContextClass();

  /**
   * Název třídy v contextu.
   *
   * @return Název.
   */
  String getName();

  /**
   * Je třída označena jako lazy? Pokud ne, tak bude její instance vytvořena automaticky po načtení contextu.
   *
   * @return True, pokud je lazy.
   */
  boolean isLazy();

  /**
   * Pořadí načtení lazy=False context tříd.
   *
   * @return Číslo pořadí (menší číslo => vyšší priorita).
   */
  int getLazyOrder();

  /**
   * Získá vytvořenou instanci třídy. Tato metoda nevytváři novou instanci.
   *
   * @return Instance nebo null.
   */
  Object getInstance();
}
