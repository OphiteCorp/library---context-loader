package oc.mimic.lib.contextloader;

import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Listener událostí pro context loader.
 *
 * @author mimic
 */
public interface IContextLoaderEventListener {

  /**
   * Byl zahájen sken package.
   *
   * @param ctx         Instance contextu.
   * @param packageName Cesta k package.
   */
  void packageScanStarted(IContextLoader ctx, String packageName);

  /**
   * Výsledek skenování package.
   *
   * @param ctx         Instance contextu.
   * @param packageName Cesta k package.
   * @param classes     Kolekce tříd v package (obsahují i třídy, které nejsou napojeny na context).
   */
  void packageScanComplete(IContextLoader ctx, String packageName, List<Class<?>> classes);

  /**
   * Byla nalezena context třída.
   *
   * @param ctx              Instance contextu.
   * @param contextClassData Informace o třídě, která půjde do contextu.
   */
  void contextClassFound(IContextLoader ctx, IContextClassData contextClassData);

  /**
   * Bylo zahájeno vytváření instance třídy.
   *
   * @param ctx              Instance contextu.
   * @param contextClassData Informace o třídě, pro kterou se má vytvořit instance.
   * @param constructor      Konstruktor, přes který se vytvoří instance třídy.
   */
  void creatingInstance(IContextLoader ctx, IContextClassData contextClassData, Constructor<?> constructor);

  /**
   * Instance třídy byla vytvořena.
   *
   * @param ctx              Instance contextu.
   * @param contextClassData Informace o třídě, pro kterou se vytvářela instance.
   * @param constructor      Konstruktor, přes který se vytvářela instance třídy.
   */
  void instanceCreated(IContextLoader ctx, IContextClassData contextClassData, Constructor<?> constructor);

  /**
   * Zahájeno volání metody s anotací {@link PostConstruct}.
   *
   * @param ctx              Instance contextu.
   * @param contextClassData Informace o hlavní třídě v contextu.
   * @param clazz            Třída s metodou, která se má zavolat.
   * @param method           Metoda s anotací {@link PostConstruct}.
   */
  void callPostConstruct(IContextLoader ctx, IContextClassData contextClassData, Class<?> clazz, Method method);

  /**
   * Zahájeno volání metody s anotací {@link PostConstruct}.
   *
   * @param ctx              Instance contextu.
   * @param contextClassData Informace o hlavní třídě v contextu.
   * @param clazz            Třída s fieldem, který se má naplnit.
   * @param field            Field s anotací {@link Inject}.
   */
  void callInject(IContextLoader ctx, IContextClassData contextClassData, Class<?> clazz, Field field);
}
