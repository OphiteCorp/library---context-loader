package oc.mimic.lib.contextloader.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Hlavní anotace, se kterou pracuje context loader.
 *
 * @author mimic
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface ContextClass {

  /**
   * Vlastní název objektu v contextu. Pokud nebude vyplněn, tak se název vygeneruje.
   */
  String value() default "";

  /**
   * Definuje, zda se má vytvořit instance třídy za běhu nebo hned.
   *
   * @return True, pokud má být instance vytvořena až za běhu aplikace. False, pokud má být vytvořena hned.
   */
  boolean lazy() default true;

  /**
   * Pořadí pro automatické vytvoření instance tříd.
   *
   * @return Číslo pořadí, kde nižší číslo znamená vyšší prioritu.
   */
  int lazyOrder() default 1000000;
}

