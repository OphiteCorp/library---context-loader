package oc.mimic.lib.contextloader.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Anotace, se kterou pracuje context loader a slouží pro naplnění fieldu instancí z contextu.
 *
 * @author mimic
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.FIELD })
public @interface Inject {

  /**
   * Vlastní název objektu v contextu. Pokud nebude vyplněn, tak se vyhodnotí automaticky.
   */
  String value() default "";
}

