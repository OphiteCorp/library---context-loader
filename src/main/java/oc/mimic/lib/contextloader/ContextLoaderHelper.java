package oc.mimic.lib.contextloader;

import oc.mimic.lib.contextloader.exception.ContextLoaderException;

import java.io.File;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.jar.JarEntry;

/**
 * Pomocné metody pro práci s context loaderem.
 *
 * @author mimic
 */
final class ContextLoaderHelper {

  private static final String CLASS_FILE_EXT = ".class";

  /**
   * Získá všechny třídy z package.
   *
   * @param packageName Cesta k package.
   *
   * @return Kolekce všech tříd v package.
   */
  public static List<Class<?>> getClassesFromPackage(String packageName) {
    var classes = new ArrayList<Class<?>>();
    try {
      var classLoader = Thread.currentThread().getContextClassLoader();
      if (classLoader == null) {
        throw new ContextLoaderException("Failed to get context class loader from current thread.");
      }
      var resources = classLoader.getResources(packageName.replace('.', '/'));
      URLConnection connection;
      URL url;

      while (resources.hasMoreElements()) {
        url = resources.nextElement();
        connection = url.openConnection();

        if (connection instanceof JarURLConnection) {
          // aplikace byla spuštěna z jar souboru
          scanJarFile((JarURLConnection) connection, packageName, classes);

        } else if (connection.getClass().getName().equals("sun.net.www.protocol.file.FileURLConnection")) {
          // aplikace byla spuštěna z IDE
          scanDirectory(new File(URLDecoder.decode(url.getPath(), StandardCharsets.UTF_8)), packageName, classes);
        } else {
          throw new ContextLoaderException(packageName + " (" + url.getPath() + ") is not a valid package.");
        }
      }
    } catch (Exception e) {
      throw new ContextLoaderException(
              "An unexpected error occurred while getting classes from package: " + packageName, e);
    }
    return classes;
  }

  /**
   * Zjistí, zda vstupní třída má nějakého rodiče.
   *
   * @param clazz Vstupní třída.
   *
   * @return True, pokud má rodiče.
   */
  public static boolean hasSuperClass(Class<?> clazz) {
    return (clazz.getSuperclass() != null && !clazz.getSuperclass().equals(Object.class));
  }

  /**
   * Vytvoří unikátní hash pro třídu.
   *
   * @return Unikátní hash třídy.
   */
  public static String createClassHash(Class<?> clazz) {
    try {
      var digest = MessageDigest.getInstance("SHA-256");
      var hash = digest.digest(clazz.getName().getBytes(StandardCharsets.UTF_8));
      return convertBytesToHex(hash);
    } catch (Exception e) {
      throw new ContextLoaderException("Error creating hash from class: " + clazz.getName(), e);
    }
  }

  /**
   * Vyhledá všechny třídy v package (pro IDE).
   *
   * @param directory   Adresář.
   * @param packageName Cesta k package.
   * @param classes     In/Out třídy.
   *
   * @throws ClassNotFoundException
   */
  private static void scanDirectory(File directory, String packageName, List<Class<?>> classes)
          throws ClassNotFoundException {

    if (directory.exists() && directory.isDirectory()) {
      var files = directory.list();
      File tempDirectory;

      for (var file : files) {
        if (file.endsWith(CLASS_FILE_EXT)) {
          try {
            classes.add(Class.forName(packageName + '.' + file.substring(0, file.length() - 6)));
          } catch (NoClassDefFoundError e) {
            // nic, ignorujeme
          }
        } else if ((tempDirectory = new File(directory, file)).isDirectory()) {
          // pokud se jedná o adresář, tak proskenujeme i ten
          scanDirectory(tempDirectory, packageName + '.' + file, classes);
        }
      }
    }
  }

  /**
   * Vyhledá všechny třídy v package (pro JAR soubor).
   *
   * @param connection  Připojení (může být do adresáře nebo jar souboru).
   * @param packageName Cesta k package.
   * @param classes     In/Out třídy.
   *
   * @throws ClassNotFoundException
   * @throws IOException
   */
  private static void scanJarFile(JarURLConnection connection, String packageName, List<Class<?>> classes)
          throws ClassNotFoundException, IOException {

    var jarFile = connection.getJarFile();
    var entries = jarFile.entries();
    JarEntry jarEntry;

    while (entries.hasMoreElements()) {
      jarEntry = entries.nextElement();
      var name = jarEntry.getName();

      if (name.contains(CLASS_FILE_EXT)) {
        name = name.substring(0, name.length() - 6).replace('/', '.');

        if (name.contains(packageName)) {
          classes.add(Class.forName(name));
        }
      }
    }
  }

  /**
   * Převede pole bytů na HEX řetězec.
   *
   * @param bytes Pole bytů.
   *
   * @return HEX řetězec.
   */
  private static String convertBytesToHex(byte[] bytes) {
    var hexString = new StringBuffer();

    for (var b : bytes) {
      var hex = Integer.toHexString(0xff & b);
      if (hex.length() == 1) {
        hexString.append('0');
      }
      hexString.append(hex);
    }
    return hexString.toString();
  }
}
