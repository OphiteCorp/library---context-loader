package oc.mimic.lib.contextloader;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.List;

/**
 * Adaptér pro události nad context loaderem.
 *
 * @author mimic
 */
public class ContextLoaderEventAdapter implements IContextLoaderEventListener {

  @Override
  public void packageScanStarted(IContextLoader ctx, String packageName) {
  }

  @Override
  public void packageScanComplete(IContextLoader ctx, String packageName, List<Class<?>> classes) {
  }

  @Override
  public void contextClassFound(IContextLoader ctx, IContextClassData contextClassData) {
  }

  @Override
  public void creatingInstance(IContextLoader ctx, IContextClassData contextClassData, Constructor<?> constructor) {
  }

  @Override
  public void instanceCreated(IContextLoader ctx, IContextClassData contextClassData, Constructor<?> constructor) {
  }

  @Override
  public void callPostConstruct(IContextLoader ctx, IContextClassData contextClassData, Class<?> clazz, Method method) {
  }

  @Override
  public void callInject(IContextLoader ctx, IContextClassData contextClassData, Class<?> clazz, Field field) {
  }
}
