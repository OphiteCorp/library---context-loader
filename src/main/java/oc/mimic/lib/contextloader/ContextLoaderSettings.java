package oc.mimic.lib.contextloader;

import oc.mimic.lib.contextloader.exception.ContextLoaderException;

import java.util.HashMap;
import java.util.Map;

/**
 * Nastavení pro {@link ContextLoader}.
 *
 * @author mimic
 */
public final class ContextLoaderSettings {

  private final Map<Class<?>, Object> customParamMapping;
  private boolean generateUniqueNames;
  private boolean ignoreNullReferences;
  private LazyMode lazyMode = LazyMode.NORMAL;

  /**
   * Vytvoří novou instanci.
   */
  public ContextLoaderSettings() {
    customParamMapping = new HashMap<>(0);
  }

  /**
   * Přidá mapování pro třídu. Tyto třídy nemusí být (neměli by být) součástí contextu.
   *
   * @param clazz    Vlastní třída.
   * @param instance Instance třídy. Může být i null.
   */
  public void addParamMapping(Class<?> clazz, Object instance) {
    if (getCustomParamMapping().containsKey(clazz)) {
      throw new ContextLoaderException("Mapping for class '" + clazz.getName() + "' already exists.");
    }
    getCustomParamMapping().put(clazz, instance);
  }

  /**
   * Vlastní mapování parametrů. Pokud parametr nebude existovat v contextu, tak se podívá sem.
   * Klíčem je typ třídy parametru a hodnotou instance třídy.
   *
   * @return Mapování.
   */
  public Map<Class<?>, Object> getCustomParamMapping() {
    return customParamMapping;
  }

  /**
   * Mají se generovat unikátní názvy tříd v contextu? Pokud nějaká třída používá vlastní název, tak ho ponechá.
   *
   * @return True, pokud ano.
   */
  public boolean isGenerateUniqueNames() {
    return generateUniqueNames;
  }

  /**
   * Mají se generovat unikátní názvy tříd v contextu? Pokud nějaká třída používá vlastní název, tak ho ponechá.
   *
   * @param generateUniqueNames True, pokud ano.
   */
  public void setGenerateUniqueNames(boolean generateUniqueNames) {
    this.generateUniqueNames = generateUniqueNames;
  }

  /**
   * Má se ignorovat field nebo parametr, pro který nebyla nalezena reference?
   *
   * @return True, pokud ano.
   */
  public boolean isIgnoreNullReferences() {
    return ignoreNullReferences;
  }

  /**
   * Má se ignorovat field nebo parametr, pro který nebyla nalezena reference?
   *
   * @param ignoreNullReferences True, pokud ano.
   */
  public void setIgnoreNullReferences(boolean ignoreNullReferences) {
    this.ignoreNullReferences = ignoreNullReferences;
  }

  /**
   * Získá nastavení pro načtení lazy tříd.
   *
   * @return Typ načtení.
   */
  public LazyMode getLazyMode() {
    return lazyMode;
  }

  /**
   * Nastaví načtení lazy tříd.
   *
   * @param lazyMode Typ načtení.
   */
  public void setLazyMode(LazyMode lazyMode) {
    this.lazyMode = lazyMode;
  }
}
