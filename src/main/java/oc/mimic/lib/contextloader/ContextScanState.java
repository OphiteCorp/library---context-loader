package oc.mimic.lib.contextloader;

/**
 * Stav průběhu skenování.
 *
 * @author mimic
 */
public enum ContextScanState {
  /**
   * Sken je připraven pro skenování package.
   */
  READY,
  /**
   * Probíhá skenování.
   */
  SCANNING,
  /**
   * Sken byl dokončen.
   */
  DONE
}
