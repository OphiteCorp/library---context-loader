package oc.mimic.lib.contextloader;

/**
 * Rozhraní pro context loader, které je voláno ve chvíli, kdy je context plně načten a připraven k použití.
 *
 * @author mimic
 */
public interface IContextLoaderReadyCallback {

  /**
   * Context byl načten.
   *
   * @param ctx Instance contextu.
   */
  void contextReady(IContextLoader ctx);
}
