package oc.mimic.lib.contextloader;

import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;
import oc.mimic.lib.contextloader.exception.ContextLoaderEventException;
import oc.mimic.lib.contextloader.exception.ContextLoaderException;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Control pro ovládání událostí pro {@link ContextLoader}.
 *
 * @author mimic
 */
final class ContextLoaderEventControl {

  // listenery pro odchycení událostí během skenování package
  private final Set<IContextLoaderEventListener> listeners;
  private final IContextLoader contextLoader;

  /**
   * Vytvoří novou instanci controlu.
   *
   * @param contextLoader Instance context loaderu.
   */
  public ContextLoaderEventControl(IContextLoader contextLoader) {
    this.contextLoader = contextLoader;
    listeners = new HashSet<>(0);
  }

  /**
   * Přidá listener pro zachycení událostí skenu. Nepodporuje přidání více stejných instancí.
   *
   * @param listener Instance listeneru.
   */
  public synchronized void addEventListener(IContextLoaderEventListener listener) {
    if (listener == null) {
      throw new ContextLoaderException("Input listener cannot be null.");
    }
    listeners.add(listener);
  }

  /**
   * Odebere listener pro zachycení událostí skenu.
   *
   * @param listener Instance listeneru.
   *
   * @return True, pokud byl listener odebrán.
   */
  public synchronized boolean removeEventListener(IContextLoaderEventListener listener) {
    if (listener == null) {
      throw new ContextLoaderException("Input listener cannot be null.");
    }
    return listeners.remove(listener);
  }

  /**
   * Událost - byl zahájen sken package.
   *
   * @param packageName Cesta k package.
   */
  public synchronized void firePackageScanStarted(String packageName) {
    for (var listener : listeners) {
      try {
        listener.packageScanStarted(contextLoader, packageName);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: packageScanStarted", e);
      }
    }
  }

  /**
   * Událost - skenování package bylo dokončeno.
   *
   * @param packageName Cesta k package.
   * @param classes     Kolekce tříd v package (obsahují i třídy, které nejsou a ani nemusí být napojeny na context).
   */
  public synchronized void firePackageScanComplete(String packageName, List<Class<?>> classes) {
    for (var listener : listeners) {
      try {
        listener.packageScanComplete(contextLoader, packageName, classes);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: packageScanComplete", e);
      }
    }
  }

  /**
   * Událost - byla nalezena context třída.
   *
   * @param contextClassData Informace o třídě, která půjde do contextu.
   */
  public synchronized void fireContextClassFound(IContextClassData contextClassData) {
    for (var listener : listeners) {
      try {
        listener.contextClassFound(contextLoader, contextClassData);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: contextClassFound", e);
      }
    }
  }

  /**
   * Událost - byla zahájeno vytváření instance třídy.
   *
   * @param contextClassData Informace o třídě, pro kterou se má vytvořit instance.
   * @param constructor      Konstruktor, přes který se vytvoří instance třídy.
   */
  public synchronized void fireCreatingInstance(IContextClassData contextClassData, Constructor<?> constructor) {
    for (var listener : listeners) {
      try {
        listener.creatingInstance(contextLoader, contextClassData, constructor);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: creatingInstance", e);
      }
    }
  }

  /**
   * Událost - instance třídy byla vytvořena.
   *
   * @param contextClassData Informace o třídě, pro kterou se vytvořila instance.
   * @param constructor      Konstruktor, přes který se vytvořila instance třídy.
   */
  public synchronized void fireInstanceCreated(IContextClassData contextClassData, Constructor<?> constructor) {
    for (var listener : listeners) {
      try {
        listener.instanceCreated(contextLoader, contextClassData, constructor);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: instanceCreated", e);
      }
    }
  }

  /**
   * Událost - bylo zahájeno volání metody s anotací {@link PostConstruct}.
   *
   * @param contextClassData Informace o hlavní třídě v contextu.
   * @param clazz            Třída s metodou, která se má zavolat.
   * @param method           Metoda s anotací {@link PostConstruct}.
   */
  public synchronized void fireCallPostConstruct(IContextClassData contextClassData, Class<?> clazz, Method method) {
    for (var listener : listeners) {
      try {
        listener.callPostConstruct(contextLoader, contextClassData, clazz, method);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: callPostConstruct", e);
      }
    }
  }

  /**
   * Událost - byla zahájeno naplnění fieldu s anotací {@link Inject}.
   *
   * @param contextClassData Informace o hlavní třídě v contextu.
   * @param clazz            Třída s fieldem, který se má naplnit.
   * @param field            Field s anotací {@link Inject}.
   */
  public synchronized void fireCallInject(IContextClassData contextClassData, Class<?> clazz, Field field) {
    for (var listener : listeners) {
      try {
        listener.callInject(contextLoader, contextClassData, clazz, field);

      } catch (Exception e) {
        throw new ContextLoaderEventException("An error occurred while processing the event: callInject", e);
      }
    }
  }
}
