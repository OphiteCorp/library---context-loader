package oc.mimic.lib.contextloader.test.data2;

import oc.mimic.lib.contextloader.annotation.ContextClass;
import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;
import oc.mimic.lib.contextloader.test.data1.IA;

@ContextClass(value = "c1", lazy = false, lazyOrder = 0)
public class C1 extends CAbstract {

  @Inject
  private IA ia;

  @PostConstruct
  private void init(C1 c1) {
    if (ia == null || mc == null) {
      throw new IllegalArgumentException();
    }
    if (c1 == null) {
      throw new IllegalArgumentException();
    }
  }
}
