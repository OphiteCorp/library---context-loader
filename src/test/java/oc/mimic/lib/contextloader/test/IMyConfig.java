package oc.mimic.lib.contextloader.test;

/**
 * @author mimic
 */
public interface IMyConfig {

  String getName();

  int getAge();
}
