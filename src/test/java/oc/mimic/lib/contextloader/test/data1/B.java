package oc.mimic.lib.contextloader.test.data1;

import oc.mimic.lib.contextloader.annotation.ContextClass;
import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;
import oc.mimic.lib.contextloader.test.IMyConfig;
import oc.mimic.lib.contextloader.test.IUid;
import oc.mimic.lib.contextloader.test.data2.C1;
import oc.mimic.lib.contextloader.test.data2.IC;

@ContextClass
public class B extends ASuperAbstract implements IB, IUid {

  private static long uid = 0;

  @Inject
  private IA a;

  @Inject("c1")
  private IC ic1;

  @Inject("c2")
  private IC ic2;

  public B() {
    ++uid;
  }

  @PostConstruct
  private void init(B b, C1 c1, IB ib) {
    if (a == null || ic1 == null || ic2 == null) {
      throw new IllegalArgumentException();
    }
    if (b == null || c1 == null || ib == null) {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public long getUid() {
    return uid;
  }

  @ContextClass("innerC")
  private static final class InnerC implements IInnerC {

    @ContextClass("my")
    private static final class My implements IMy {

      private final int value = 1337;

      @PostConstruct
      private void myInit(IInnerC innerC) {
        if (innerC == null) {
          throw new IllegalArgumentException();
        }
      }

      @Override
      public int getValue() {
        return value;
      }

      @ContextClass(value = "deep", lazy = false)
      private static final class Deep {

        @Inject("my")
        private IMy my;

        @Inject
        private IMyConfig mc;

        private Deep(IMyConfig mc) {
          if (mc == null) {
            throw new IllegalArgumentException();
          }
        }

        @PostConstruct
        private void init(IMyConfig mc) {
          if (my == null || this.mc == null) {
            throw new IllegalArgumentException();
          }
          if (mc == null) {
            throw new IllegalArgumentException();
          }
        }
      }
    }
  }
}
