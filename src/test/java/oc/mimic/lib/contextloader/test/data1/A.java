package oc.mimic.lib.contextloader.test.data1;

import oc.mimic.lib.contextloader.annotation.ContextClass;
import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;
import oc.mimic.lib.contextloader.test.IMyConfig;
import oc.mimic.lib.contextloader.test.IUid;

@ContextClass
public class A extends AAbstract implements IA, IUid {

  private static long uid = 0;

  @Inject
  private IB ib;

  public A(IMyConfig mc) {
    if (mc == null) {
      throw new IllegalArgumentException();
    }
    ++uid;
  }

  @PostConstruct
  private void init1(IA ia, A a, IB ib, IMy my) {
    if (this.ib == null) {
      throw new IllegalArgumentException();
    }
    if (ia == null || a == null || ib == null || my == null) {
      throw new IllegalArgumentException();
    }
  }

  @PostConstruct
  private void init2(IA ia, IInnerC innerC, IMy my) {
    if (ia == null || innerC == null || my == null) {
      throw new IllegalArgumentException();
    }
  }

  @Override
  public long getUid() {
    return uid;
  }
}
