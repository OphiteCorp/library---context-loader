package oc.mimic.lib.contextloader.test.data2;

import oc.mimic.lib.contextloader.annotation.ContextClass;
import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;
import oc.mimic.lib.contextloader.test.data1.IB;

@ContextClass(value = "c2")
public class C2 extends CAbstract {

  @Inject
  private IB ib;

  @PostConstruct
  private void init(C2 c2) {
    if (ib == null || mc == null) {
      throw new IllegalArgumentException();
    }
    if (c2 == null) {
      throw new IllegalArgumentException();
    }
  }

  @ContextClass(lazy = false)
  private static final class C2Exec extends CAbstract {

    @PostConstruct
    private void init(C2 c2) {
      if (c2 == null || mc == null) {
        throw new IllegalArgumentException();
      }
    }
  }
}
