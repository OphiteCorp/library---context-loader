package oc.mimic.lib.contextloader.test;

import oc.mimic.lib.contextloader.*;
import oc.mimic.lib.contextloader.test.data1.IA;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.List;

/**
 * Example run class.
 *
 * @author mimic
 */
public final class Application {

  public static void main(String[] args) {

    var settings = new ContextLoaderSettings();
    /*
     * Uses a unique hash for the class name in conext.
     * If the class does not have its own name in ContextClass, it will generate a unique hash.
     * The default value is False.
     */
    settings.setGenerateUniqueNames(false);
    /*
     * Ignores NULL references.
     * This means that if False (which is the default), it is not possible to initialize a value with null.
     * There must always be an instance. For Inject, parameters in the constructor and PostConstruct methods.
     */
    settings.setIgnoreNullReferences(false);
    /*
     * lazy load mode.
     * NORMAL = Everything will work normally. This means that lazy=False automatically instantiates the class
     * with everything. This is the default value.
     * NEGATE = It works in the opposite way as NORMAL. This means that lazy=False behaves like lazy=True
     * (which is the default) and everything else will be loaded automatically.
     * ALL = Everything will behave like lazy=False.
     */
    settings.setLazyMode(LazyMode.NORMAL);
    /*
     * Custom mapping between class type and instance.
     * Used for 2 things. The first is that this mapping can be used in constructors to create an instance of classes.
     * Here, these classes can be used as parameters in the constructor.
     * The second case is that these classes can be treated in the same way as classes in the context.
     * They can be used for both Inject and PostConstruct parameters.
     * There is no mapping by default.
     */
    settings.addParamMapping(IMyConfig.class, new MyConfig());

    var loader = ContextLoader.getInstance();
    loader.setSettings(settings);
    // Listener for tapping events around context loader. This listener is not required,
    // it is only for logging or for specific use.
    loader.addEventListener(new ContextLoaderEvents());
    // This callback is important because it is called when the scan and class instances are complete.
    // There should be logic or application startup.
    // It is not necessary if any context class is set as lazy=False and has application logic in it.
    loader.setReadyCallback(ctx -> {
      System.out.println("State: " + ctx.getContextState());
      System.out.println("Loaded classes: " + Arrays.toString(ctx.getAllContexts().toArray()));
      // gets an instance of the class (if it does not exist, it creates it)
      // parameter: It can be used as an interface, implementation, or class name in the context
      ctx.get(IA.class);
    });
    // Starts the package scan. There may be more packages. ReadyCallback is called when the scan is complete.
    loader.scan("oc.mimic.lib.contextloader.test.data1", "oc.mimic.lib.contextloader.test.data2");
  }

  /**
   * Listener instance for logging to the console. It is not needed in production deployment.
   */
  private static final class ContextLoaderEvents extends ContextLoaderEventAdapter {

    @Override
    public void packageScanStarted(IContextLoader ctx, String packageName) {
      System.out.println("Package scan started: " + packageName + " -> " + ctx.getContextState());
    }

    @Override
    public void packageScanComplete(IContextLoader ctx, String packageName, List<Class<?>> classes) {
      for (Class<?> clazz : classes) {
        System.out.println("  - Scan '" + packageName + "' complete: [" + clazz.getSimpleName() + "] -> valid: " +
                           ctx.isValidContextClass(clazz));
      }
    }

    @Override
    public void contextClassFound(IContextLoader ctx, IContextClassData contextClassData) {
      System.out.println("    > Context class found: " + contextClassData.getContextClass().getName() + " (" +
                         contextClassData.getName() + ")");
    }

    @Override
    public void creatingInstance(IContextLoader ctx, IContextClassData contextClassData, Constructor<?> constructor) {
      System.out.println("  - Creating instance: " + contextClassData.getContextClass().getSimpleName() +
                         parametersToString(constructor.getParameters()));
    }

    @Override
    public void instanceCreated(IContextLoader ctx, IContextClassData contextClassData, Constructor<?> constructor) {
      var uid = -999L;
      if (contextClassData.getInstance() instanceof IUid) {
        uid = ((IUid) contextClassData.getInstance()).getUid();
      }
      System.out.println("  - Instance created: " + contextClassData.getContextClass().getSimpleName() +
                         parametersToString(constructor.getParameters()) + " = UID: " + uid);
    }

    @Override
    public void callPostConstruct(IContextLoader ctx, IContextClassData contextClassData, Class<?> clazz,
            Method method) {
      System.out.println("    | postConstruct: " + clazz.getName() + "#" + method.getName() +
                         parametersToString(method.getParameters()));
    }

    @Override
    public void callInject(IContextLoader ctx, IContextClassData contextClassData, Class<?> clazz, Field field) {
      System.out.println("    + inject: " + clazz.getName() + "#" + field.getName());
    }
  }

  // helper method
  private static String parametersToString(Parameter[] params) {
    var sb = new StringBuilder();

    if (params != null && params.length > 0) {
      for (var i = 0; i < params.length; i++) {
        sb.append(params[i].getType().getSimpleName());
        if (i < params.length - 1) {
          sb.append(", ");
        }
      }
    }
    return "(" + sb.toString() + ")";
  }
}
