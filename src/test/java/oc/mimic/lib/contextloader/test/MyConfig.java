package oc.mimic.lib.contextloader.test;

/**
 * @author mimic
 */
public final class MyConfig implements IMyConfig {

  private final String name = "mimic";
  private final int age = 999;

  @Override
  public String getName() {
    return name;
  }

  @Override
  public int getAge() {
    return age;
  }
}
