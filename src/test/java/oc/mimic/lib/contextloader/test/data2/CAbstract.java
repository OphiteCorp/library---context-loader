package oc.mimic.lib.contextloader.test.data2;

import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.test.IMyConfig;
import oc.mimic.lib.contextloader.test.IUid;

/**
 * @author mimic
 */
abstract class CAbstract implements IC, IUid {

  private static long uid = 0;

  @Inject
  protected IMyConfig mc;

  protected CAbstract() {
    ++uid;
  }

  @Override
  public long getUid() {
    return uid;
  }
}
