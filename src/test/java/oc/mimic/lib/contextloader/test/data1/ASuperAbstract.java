package oc.mimic.lib.contextloader.test.data1;

import oc.mimic.lib.contextloader.annotation.Inject;
import oc.mimic.lib.contextloader.annotation.PostConstruct;

public abstract class ASuperAbstract {

  @Inject
  private IMy my;

  @PostConstruct
  private void aSuperInit1() {
    if (my == null) {
      throw new IllegalArgumentException();
    }
  }

  @PostConstruct
  private void aSuperInit2(IB ib) {
    if (ib == null) {
      throw new IllegalArgumentException();
    }
  }
}
