package oc.mimic.lib.contextloader.test;

/**
 * @author mimic
 */
public interface IUid {

  long getUid();
}
